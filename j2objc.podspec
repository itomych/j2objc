@version = "1.2.0.1"

Pod::Spec.new do |s|
  s.name         		= "j2objc"
  s.version      		= @version
  s.summary      		= "Frameworked version of J2ObjC"
  s.homepage        = "https://itomychstudio.com"
  s.license         = { :type => 'MIT', :file => 'LICENSE' }
  s.author       		= { "iTomych Studio" => "office@itomy.ch" }
  s.source          = { :git => "https://bitbucket.org/itomych/j2objc.git", :tag => s.version }

  s.platform     		= :ios, "8.0"
  s.requires_arc 		= true

  s.frameworks      = 'Security'
  s.libraries       = 'icucore', 'z', 'iconv'

  s.prepare_command = <<-CMD
      Scripts/download.sh
  CMD

  s.preserve_paths = ['Distributive']

  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/j2objc/Distributive/frameworks"', 'HEADER_SEARCH_PATHS' => '"$(PODS_ROOT)/j2objc/Distributive/frameworks/JRE.framework/Headers"', 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/j2objc/Distributive/lib"' } 


  
end